-- Заполнение таблицы line

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (1, 'Сокольническая', 'Красный', '05-15-1935', '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (2, 'Замоскворецкая', 'Зеленый', '09-11-1938', '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (3, 'Арбатско-Покровская', 'Синий', '03-13-1938', '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (4, 'Филёвская', 'Голубой', '11-07-1958', '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (5, 'Кольцевая', 'Коричневый', '01-01-1950', '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (6, 'Калужско-Рижская', 'Оранжевый', '05-01-1958', '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (7, 'Таганско-Краснопресненская', 'Фиолетовый', '11-07-1966',
        '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (9, 'Серпуховско-Тимирязевская', 'Серый', '11-08-1983', '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (10, 'Люблинско-Дмитровская', 'Салатовый', '12-28-1995', '12-31-9999');

insert into moscow_metro_database.line (line_no, line_nm, color,
                                        opened_since_dt, closed_after_dt)
values (11, 'Большая кольцевая', 'Бирюзовый', '02-26-2018', '12-31-9999');

-- Заполнение таблицы train_type

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-775/776/777 «Москва-2020»', '01-01-2020', '01-01-2023');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-775.2/776.2/777.2 «Москва-2024»', '01-01-2023', '12-31-9999');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-765.4/766.4/767.4 «Москва-2019»', '01-01-2019', '01-01-2020');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-765/766/767 «Москва»', '01-01-2016', '01-01-2020');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-760/761 «Ока»', '01-01-2010', '01-01-2016');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-740.4/741.4 «Русич»', '01-01-2008', '01-01-2013');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-740.1/741.1 «Русич»', '01-01-2004', '01-01-2009');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-717.6/714.6', '01-01-2009', '01-01-2011');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-717.5/714.5', '01-01-1988', '01-01-1996');

insert into moscow_metro_database.train_type (train_nm, prodicing_since_dt,
                                              prodicing_upto_dt)
values ('81-717.5М/714.5М', '01-01-1993', '01-01-2010');

-- Заполнение таблицы Train

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (10, 9, '01-01-1988', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (2, 9, '01-01-1988', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (10, 10, '01-01-1993', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (1, 10, '01-01-1993', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (2, 10, '01-01-1993', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (2, 2, '01-01-2024', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (5, 1, '01-01-2020', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (6, 1, '01-01-2020', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (11, 1, '01-01-2020', '12-31-9999');

insert into moscow_metro_database.train (line_no, type_id, operating_from_dt,
                                         operating_upto_dt)
values (3, 6, '01-01-2009', '12-31-9999');

-- Заполнение таблицы Transfer

insert into moscow_metro_database.transfer (id, count)
values (1, 0),
       (2, 0),
       (3, 0),
       (4, 0),
       (5, 0),
       (6, 0),
       (7, 0),
       (8, 0),
       (9, 0),
       (10, 0);

-- Заполнение таблицы Station

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Парк культуры', '01-01-1950', '01-01-9999', 6, 'Открыта', '5:30', 1);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Киевская', '03-14-1954', '01-01-9999', 6, 'Открыта', '5:30', 2);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Краснопресненская', '03-14-1954', '01-01-9999', 6, 'Открыта', '5:30', 3);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Белорусская', '01-30-1952', '01-01-9999', 6, 'Открыта', '5:30', 4);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Новослободская', '01-30-1952', '01-01-9999', 6, 'Открыта', '5:30', 5);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Библиотека имени Ленина', '05-15-1935', '01-01-9999', 1, 'Открыта', '5:30', 6);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Боровицкая', '01-23-1986', '01-01-9999', 9, 'Открыта', '5:30', 6);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Александровский сад', '11-07-1958', '01-01-9999', 4, 'Открыта', '5:30', 6);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Арбатская', '04-05-1950', '01-01-9999', 3, 'Открыта', '5:30', 6);

insert into moscow_metro_database.station (station_nm, opened_since_dt,
                                           closed_after_dt, line_no, status,
                                           opening_time_tm, transfer_id)
values ('Физтех', '09-07-2023', '01-01-9999', 10, 'Открыта', '5:30', null);