-- View запросы

-- Все станции с названиями линий

create view stations_with_lines as
select station_nm, station.opened_since_dt, status, line_nm
from moscow_metro_database.station
       inner join moscow_metro_database.line l on l.line_no = station.line_no;

-- Все кодовые номера поездов с датой начала их производства

create view train_numbers as
select substring(train_nm, 1, 6) as number, prodicing_since_dt
from moscow_metro_database.train_type;

-- Действующие поезда на линиях метро (с названием поезда и названием линии)

create view trains_at_lines as
select line_nm, train_nm, operating_from_dt
from moscow_metro_database.train
       inner join moscow_metro_database.line l on l.line_no = train.line_no
       inner join moscow_metro_database.train_type tt on tt.id = train.type_id;

-- Все станции, открытые за последние 30 лет

create view modern_stations as
select station_nm, opened_since_dt, line_no, transfer_id
from moscow_metro_database.station
where age(opened_since_dt)::interval < interval '30 years';



