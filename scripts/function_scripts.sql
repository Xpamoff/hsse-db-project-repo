-- Функции, процедуры, триггеры

create extension plpython3u;

-- Выводит все станции открытые до определенной даты

create or replace function stations_opened_before_(d date)
  returns table
          (
            name text,
            line text
          )
as
$$
select station_nm, line_no
from moscow_metro_database.station
where opened_since_dt < d;
$$ language sql;

select stations_opened_before_('10-10-1960');

-- Вывоводит все поезда, где начало номера поезда совпадает с введенным пользователем значением

create or replace function trains_by_partial_number(t text)
  returns table
          (
            name            text,
            producing_since text
          )
as
$$
  ans = plpy.execute(f"select train_nm as name, prodicing_since_dt as producing_since from moscow_metro_database.train_type where substring(train_nm, 1, {len(t)}::integer) like ('{t}'::text);")
  return ans
  $$ language plpython3u;


select trains_by_partial_number('81-77');

-- Процедура закрытия станции (не навсегда)

create or replace procedure close_station(name text)
as
$$
update moscow_metro_database.station
set status = 'Закрыта'
where station_nm = name;
$$
  language sql;

call close_station('Физтех');

-- Триггер, который при добавлении новой станции в пересадку увеличит счётчик transfer.count - количество станций содержащихся в пересадке

create or replace function update_transfer() returns trigger as
$$
begin
  if new.transfer_id is null
  then
    return new;
  else
    update moscow_metro_database.transfer
    set count = count + 1
    where id = new.transfer_id;
    return new;
  end if;
end;
$$
  language plpgsql;

create or replace trigger updater
  before insert or update
  on moscow_metro_database.station
  for each row
execute function update_transfer();
