-- Select запросы

-- Названия типов поездов, которые ездят по 2 линии

select distinct tt.train_nm, 
                operating_from_dt, 
                line_no
from moscow_metro_database.train
       inner join moscow_metro_database.train_type tt on tt.id = train.type_id
where line_no = 2
order by train_nm;

-- Количесто поездов на разных линиях метро

select line_no, 
       count(*)
from moscow_metro_database.train
group by line_no
order by line_no;

-- Список станций, расположенных на 5 линии метро

select l.line_nm, 
       station_nm, 
       l.opened_since_dt
from moscow_metro_database.station
       inner join moscow_metro_database.line l on l.line_no = station.line_no
where l.line_no = 5
order by station_nm;

-- Станции открытые за последние 70 лет

select station_nm, 
       line_no, 
       opened_since_dt
from moscow_metro_database.station
where age(opened_since_dt::timestamp) < interval '70 years'
order by opened_since_dt;

-- Все станции расположенные на 6 пересадке

select station_nm, 
       line_no
from moscow_metro_database.transfer
       inner join moscow_metro_database.station s on transfer.id = s.transfer_id
where transfer_id = 6
order by station_nm;

-- Список типов поездов, выпущенных в одинаковые даты

select row_number() over (partition by prodicing_upto_dt),
       tt.train_nm,
       prodicing_upto_dt
from moscow_metro_database.train
       inner join moscow_metro_database.train_type tt on tt.id = train.type_id
order by prodicing_upto_dt;

-- Количество станций на определённых линиях метро (с названием станции)

select count(*) over (partition by l.line_no) as amount, 
       station_nm, 
       l.line_no
from moscow_metro_database.station
       inner join moscow_metro_database.line l on l.line_no = station.line_no
order by amount desc;

-- Даты открытия самых первых станций на определенной линии метро

select first_value(moscow_metro_database.station.opened_since_dt)
       over (partition by moscow_metro_database.station.line_no order by moscow_metro_database.station.opened_since_dt) as line_opening,
       l.line_nm,
       station_nm
from moscow_metro_database.station
       inner join moscow_metro_database.line l on l.line_no = station.line_no
order by moscow_metro_database.station.opened_since_dt;

-- Количество поездов потипово на каждой линии метро

select moscow_metro_database.train.id,
       train_nm,
       count(*) over (partition by l.line_no) as train_amount,
       line_nm
from moscow_metro_database.train
       inner join moscow_metro_database.train_type tt on tt.id = train.type_id
       inner join moscow_metro_database.line l on l.line_no = train.line_no
order by l.line_no;

-- Количество станций на пересадочном узле

select transfer.id,
       station_nm,
       count(*) over (partition by transfer.id) as stations_at_transfer
from moscow_metro_database.transfer
       inner join moscow_metro_database.station s on transfer.id = s.transfer_id
order by stations_at_transfer desc;
