-- Update/Delete запросы

update moscow_metro_database.station
set line_no     = '4',
    transfer_id = null
where station_nm = 'Арбатская';

update moscow_metro_database.train_type
set prodicing_since_dt = '2009-01-01'
where id = 6;

update moscow_metro_database.train
set line_no='6'
where type_id = '1';

update moscow_metro_database.station
set line_no = 5
where line_no = 6;

delete from moscow_metro_database.train
where id='9';

delete from moscow_metro_database.transfer
where id='10';