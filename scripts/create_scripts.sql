CREATE SCHEMA moscow_metro_database;

CREATE TABLE moscow_metro_database.Line
(
  line_no         integer PRIMARY KEY,
  line_nm         text NOT NULL,
  color           text NOT NULL,
  opened_since_dt date NOT NULL,
  closed_after_dt date
);

CREATE TABLE moscow_metro_database.Train_type
(
  id                 serial PRIMARY KEY,
  train_nm           text NOT NULL,
  prodicing_since_dt date NOT NULL,
  prodicing_upto_dt  date
);

CREATE TABLE moscow_metro_database.Station
(
  id              serial PRIMARY KEY,
  station_nm      text    NOT NULL,
  opened_since_dt date    NOT NULL,
  closed_after_dt date,
  line_no         integer NOT NULL,
  status          text    NOT NULL,
  opening_time_tm time    NOT NULL,
  transfer_id     integer
);

CREATE TABLE moscow_metro_database.Train
(
  id                serial PRIMARY KEY,
  line_no           integer NOT NULL,
  type_id           integer NOT NULL,
  operating_from_dt date,
  operating_upto_dt date
);

CREATE TABLE moscow_metro_database.Transfer
(
  id    serial PRIMARY KEY,
  count integer
);

ALTER TABLE moscow_metro_database.Station
  ADD FOREIGN KEY (line_no) REFERENCES moscow_metro_database.Line (line_no);

ALTER TABLE moscow_metro_database.Train
  ADD FOREIGN KEY (line_no) REFERENCES moscow_metro_database.Line (line_no);

ALTER TABLE moscow_metro_database.Train
  ADD FOREIGN KEY (type_id) REFERENCES moscow_metro_database.Train_type (id);

ALTER TABLE moscow_metro_database.Station
  ADD FOREIGN KEY (transfer_id) REFERENCES moscow_metro_database.Transfer (id);
